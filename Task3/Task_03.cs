using System;

namespace Task_03
{
public sealed class VectorList
    {
        #region Fields
        //private Array _vector;
        private int[] _vector;
        private int _lowerBound;
        private int _upperBound;
        private int _offset;
        #endregion

        #region Constructors
        public VectorList(int count = 10)
        {
            if (count < 0)
                throw new ArgumentOutOfRangeException("count");

            //_vector = Array.CreateInstance(typeof(int), new int[] { count }, new int[] { 0 });
            // Alternative solution for minus indexes in array access.
            _vector = new int[count];
            _lowerBound = 0;
            _upperBound = count - 1;
            _offset = -_lowerBound;
        }

        // Creates an array of specified limits.
        public VectorList(int lowerBound, int upperBound)
        {
            if (upperBound - lowerBound < 0)
                throw new ArgumentOutOfRangeException("higherBound", "upperBound parameter must be bigger than lowerBound parameter or be equal");

            //_vector = Array.CreateInstance(typeof(int), new int[] { upperBound - lowerBound + 1 }, new int[] { lowerBound });
            _vector = new int[upperBound - lowerBound + 1];
            _lowerBound = lowerBound;
            _upperBound = upperBound;
            _offset = -_lowerBound;
        }
        #endregion

        #region Properties
        // Returns length of array.
        public int Length
        {
            get
            {
                return _vector.Length;
            }
        }

        // Lower bound of array.
        public int LowerBound
        {
            get
            {
                //return _vector.GetLowerBound(0);
                return _lowerBound;
            }
        }

        // Upper bound of array.
        public int UpperBound
        {
            get
            {
                //return _vector.GetUpperBound(0);
                return _upperBound;
            }
        }
        #endregion

        #region Indexer
        public int this[int index]
        {
            get
            {
                // if (index < _vector.GetLowerBound(0) || index > _vector.GetUpperBound(0))
                // throw new ArgumentOutOfRangeException("index");
                if (index < LowerBound || index > UpperBound)
                    throw new ArgumentOutOfRangeException("index");

                //return (int)_vector.GetValue(index);
                return _vector[index + _offset];
            }

            set
            {
                // if (index < _vector.GetLowerBound(0) || index > _vector.GetUpperBound(0))
                // throw new ArgumentOutOfRangeException("index");
                if (index < LowerBound || index > UpperBound)
                    throw new ArgumentOutOfRangeException("index");

                //_vector.SetValue(value, index);
                _vector[index + _offset] = value;
            }
        }
        #endregion

        #region Methods
        // Add two vectors with same length.
        public static VectorList Add(VectorList vector1, VectorList vector2)
        {
            if (vector1 != vector2)
                throw new ArgumentException("Vectors must have the same range of indexes");

            var tempV = new VectorList(vector1.LowerBound, vector2.UpperBound);
            for (var i = vector1.LowerBound; i <= vector1.UpperBound; i++)
            {
                tempV[i] = vector1[i] + vector2[i];
            }

            return tempV;
        }

        public VectorList Add(VectorList vector)
        {
            if (this != vector)
                throw new ArgumentException("Vectors must have the same range of indexes");

            return VectorList.Add(this, vector);
        }

        // Substract two vectors.
        public static VectorList Substract(VectorList vector1, VectorList vector2)
        {
            if (vector1 != vector2)
                throw new ArgumentException("Vectors must have the same range of indexes");

            var tempV = new VectorList(vector1.LowerBound, vector2.UpperBound);
            for (var i = vector1.LowerBound; i <= vector1.UpperBound; i++)
            {
                tempV[i] = vector1[i] - vector2[i];
            }

            return tempV;
        }

        public VectorList Substract(VectorList vector)
        {
            if (this != vector)
                throw new ArgumentException("Vectors must have the same range of indexes");

            return VectorList.Substract(this, vector);
        }

        // Multiplication vector by value.
        public static VectorList Multiply(VectorList vector, int value)
        {
            if (vector == null)
                throw new ArgumentNullException("vector");

            var tempV = new VectorList(vector.LowerBound, vector.UpperBound);
            for (var i = vector.LowerBound; i <= vector.UpperBound; i++)
            {
                tempV[i] = vector[i] * value;
            }

            return tempV;
        }

        public VectorList Multiply(int value)
        {
            return VectorList.Multiply(this, value);
        }

        public override string ToString()
        {
            var res = "";
            bool firstElem = true;

            //foreach(var elem in _vector)
            foreach (int elem in _vector)
            {
                if (firstElem)
                {
                    res += elem;
                    firstElem = false;
                }
                else
                {
                    res += string.Format(", {0}", elem);
                }
            }
            return res;
        }

        public override bool Equals(object other)
        {
            bool result = false;

            if (other is VectorList)
            {
                result = this.Length == (other as VectorList).Length;
            }

            return result;
        }

        public override int GetHashCode()
        {
            int result = 0;
            int shift = 0;

            for (int i = LowerBound; i < UpperBound; i++)
            {
                shift = (shift + 11) % 21;
                result ^= ((int)_vector.GetValue(i) + 1024) << shift;
            }

            return result;
        }
        #endregion

        #region Operators
        // Compare lengths of vectors.
        public static bool operator <(VectorList lVector, VectorList rVector)
        {
            return (lVector.Length < rVector.Length);
        }

        public static bool operator <=(VectorList lVector, VectorList rVector)
        {
            return (lVector.Length < rVector.Length || lVector == rVector);
        }

        public static bool operator >(VectorList lVector, VectorList rVector)
        {
            return (lVector.Length > rVector.Length);
        }

        public static bool operator >=(VectorList lVector, VectorList rVector)
        {
            return (lVector.Length > rVector.Length || lVector == rVector);
        }

        // True if range of indexes are equal.
        public static bool operator ==(VectorList lVector, VectorList rVector)
        {
            if ((object)lVector == null || (object)rVector == null)
                return false;

            return (lVector.LowerBound == rVector.LowerBound && lVector.UpperBound == rVector.UpperBound);
        }

        public static bool operator !=(VectorList lVector, VectorList rVector)
        {
            if ((object)lVector == null || (object)rVector == null)
                return false;

            return !(lVector == rVector);
        }

        public static VectorList operator +(VectorList lVector, VectorList rVector)
        {
            return VectorList.Add(lVector, rVector);
        }

        public static VectorList operator -(VectorList lVector, VectorList rVector)
        {
            return VectorList.Substract(lVector, rVector);
        }

        public static VectorList operator *(VectorList vector, int value)
        {
            return VectorList.Multiply(vector, value);
        }

        public static VectorList operator *(int value, VectorList vector)
        {
            return VectorList.Multiply(vector, value);
        }
        #endregion
    }

    public class Program
    {
        static Random rnd = new Random();
        static void RandomFill(VectorList v)
        {
            for (var i = v.LowerBound; i <= v.UpperBound; i++)
            {
                v[i] = rnd.Next(-10, 10);
            }
        }

        public static void Main()
        {
            // Створення векторів.
            var v1 = new VectorList(5);
            var v2 = new VectorList(-5, 10);
            var v3 = new VectorList(-5, 10);
            VectorList res;

            RandomFill(v1);
            RandomFill(v2);
            RandomFill(v3);
            Console.WriteLine("Vector1: [{0}]", v1);
            Console.WriteLine("Vector2: [{0}]", v2);
            Console.WriteLine("Vector3: [{0}]", v3);

            // Демонстрація від'ємного індекса(без магії в індексаторі, але з упаковкою при відмінній від 0 нижній межі :( ).
            Console.WriteLine("Element of vector2 with index [-5]: {0}", v2[10]);

            // Межі масиву чисел.
            Console.WriteLine("Range of vector3 : [{0}; {1}]", v3.LowerBound, v3.UpperBound);

            // Додавання двох векторів.
            res = v2 + v3;
            Console.WriteLine("Addition of two vectors: {0}", res);

            // Віднімання двох векторів.
            res = v2 - v3;
            Console.WriteLine("Substraction of two vectors: {0}", res);

            // Множення вектора на скаляр.
            res = v1 * 2;
            Console.WriteLine("Multiplication by value: [{0}]", res);

            // Порівняння двох векторів.
            Console.WriteLine("vector1 == vector2 ? {0}", v1 == v2);
            Console.WriteLine("vector2 == vector3 ? {0}", v2 == v3);
            Console.WriteLine("vector1 < vector2 ? {0}", v1 < v2);
        }
    }
}