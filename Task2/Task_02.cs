using System;

namespace Task_02
{    
    public struct Point
    {
        private double x, y;
        
        public double X 
        { 
            get
            {
                return x;
            }
            
            set
            {
                x = value;
            }
        }

        public double Y
        {
            get
            {
                return y;
            }

            set
            {
                y = value;
            }
        }

        public Point(double x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }

    public sealed class Rectangle
    {
        #region Constructors
        public Rectangle(double x, double y, double width, double height)
        {
            if (width < 0 || height < 0)
                throw new ArgumentOutOfRangeException();

            this.X = x;
            this.Y = y;
            this.Width = width;
            this.Height = height;
        }

        public Rectangle(Point position, double width, double height): this(position.X, position.Y, width, height)
        {
            
        }
        #endregion

        #region Properties
        public double X { get; private set; }
        public double Y { get; private set; }
        public double Width { get; private set; }
        public double Height { get; private set; }

        public bool IsEmpty
        {
            get
            {
                return (Width == 0 && Height == 0 && X == 0 && Y == 0);
            }
        }

        public double Perimeter
        {
            get
            {
                return 2 * (Width + Height);
            }
        }

        public double Square
        {
            get
            {
                return Width * Height;
            }
        }
        #endregion

        #region Methods
        // Union of 2 rectangles.
        public static Rectangle Merge(Rectangle rect1, Rectangle rect2)
        {
            if (rect1 == null || rect2 == null)
                throw new ArgumentNullException();

            double x1 = Math.Min(rect1.X, rect2.X);
            double x2 = Math.Max(rect1.X + rect1.Width, rect2.X + rect2.Width);
            double y1 = Math.Min(rect1.Y, rect2.Y);
            double y2 = Math.Max(rect1.Y + rect1.Height, rect2.Y + rect2.Height);

            return new Rectangle(x1, y1, x2 - x1, y2 - y1);
        }

        // Intersect of 2 rectangles.
        public static Rectangle Intersect(Rectangle rect1, Rectangle rect2)
        {
            if (rect1 == null || rect2 == null)
                throw new ArgumentNullException();

            double x1 = Math.Max(rect1.X, rect2.X);
            double x2 = Math.Min(rect1.X + rect1.Width, rect2.X + rect2.Width);
            double y1 = Math.Max(rect1.Y, rect2.Y);
            double y2 = Math.Min(rect1.Y + rect1.Height, rect2.Y + rect2.Height);

            if (x2 >= x1 && y2 >= y1)
            {
                return new Rectangle(x1, y1, x2 - x1, y2 - y1);
            }

            return null;
        }

        // Move rectangle to new point.
        public void Move(double newX, double newY)
        {
            this.X = newX;
            this.Y = newY;
        }

        public void Move(Point newPoint)
        {
            Move(newPoint.X, newPoint.Y);
        }

        // Resize rectangle.
        public void Resize(double width, double height)
        {
            if (width < 0 || height < 0)
                throw new ArgumentOutOfRangeException();

            this.Width = width;
            this.Height = height;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            bool result = false;

            if (!(obj is Rectangle))
                result = false;

            Rectangle tempR = (Rectangle)obj;
            result = (tempR.X == this.X) && (tempR.Y == this.Y)
                     && (tempR.Width == this.Width) && (tempR.Height == this.Height);

            return result;
        }

        public override string ToString()
        {
            return String.Format("x = {0}, y = {1}, Width = {2}, Height = {3}", this.X, this.Y, this.Width, this.Height);

        }
        #endregion

        #region Operators
        public static bool operator ==(Rectangle lRect, Rectangle rRect)
        {
            if ((object)lRect == null || (object)rRect == null)
                return false;

            return (lRect.X == rRect.X && lRect.Y == rRect.Y
                    && lRect.Width == rRect.Width && lRect.Height == rRect.Height);
        }

        public static bool operator !=(Rectangle lRect, Rectangle rRect)
        {
            return !(lRect == rRect);
        }
        #endregion
    }

    public class Program
    {
        public static void Main()
        {
            // Створення об'єктів.
            var r1 = new Rectangle(1, 1, 8, 5);
            var r2 = new Rectangle(6, 3, 5, 6);
            Rectangle result;

            Console.WriteLine("1st rectangle {0}", r1);
            Console.WriteLine("2nd rectangle {0}", r2);
            Console.WriteLine();

            // Об'єднання першого і другого прямокутників.
            result = Rectangle.Merge(r1, r2);
            Console.WriteLine("Union of these rectangles: {0}", result);

            //Перетин прямокутників.
            result = Rectangle.Intersect(r1, r2);
            Console.WriteLine("Intersect of 1st & 2nd regtangles: {0}", result);

            //Переміщення першого прямокутника в задану точку.
            double x = 2, y = 8;
            r1.Move(x, y);
            //r2.Move(new Point(2, 2));
            Console.WriteLine("Moved 1st rectangle in point [{0}; {1}]: {2}", x, y, r1);

            // Зміна розмірів.
            double width = 2, height = 5;
            r2.Resize(width, height);
            Console.WriteLine("Resize 2nd rectangle in width = {0}, height = {1}: {2}", width, height, r2);

            // Перевірка рівності фігур.
            Console.WriteLine("r1 = r2 ? {0}", r1 == r2);
        }
    }
}